const jwt = require('jsonwebtoken');
const rp = require('request-promise');

module.exports = {
    createZoomMeeting: function (req, res) {
        console.log("createZoomMeeting called");
        console.log("req body --> ", req.body)

        const payload = {
            iss: process.env.ZOOM_API_KEY,
            exp: ((new Date()).getTime() + 5000)
        };
        console.log("payload --> ", payload);
        const token = jwt.sign(payload, process.env.ZOOM_API_SECRET);
        console.log("token --> ", token);


        var meeting = {
            topic: req.body.subject,
            type: 2,
            start_time: req.body.start_time,
            duration: req.body.duration,
            password: req.body.password,
            settings: {
                join_before_host: true
            }
        }

        console.log("data json --> ", meeting)

        var options = {
            method: 'POST',
            uri: "https://api.zoom.us/v2/users/k9SQsH6cRISnfCE4GqDigw/meetings",
            qs: {
                status: 'active'
            },
            body : meeting,
            auth: {
                'bearer': token
            },
            headers: {
                'User-Agent': 'Zoom-api-Jwt-Request',
                'content-type': 'application/json',
                'Content-Length': meeting.length,
            },
            json: true //Parse the JSON string in the response
        };

        rp(options).then(function (response) {
            //printing the response on the console
              console.log('User has', response);
            
          })
          .catch(function (err) {
              // API call failed...
              console.log('API call failed, reason ', err);
          });

    }
}