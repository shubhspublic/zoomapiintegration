require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const meetingRouter = require('./routes/meeting')
const jwt = require('jsonwebtoken');
let https = require('https');


app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/shubhs', meetingRouter);

(async function () {
    app.listen(process.env.PORT, process.env.HOST, function () {
        console.log("Node app is listening at http://%s:%s", process.env.HOST, process.env.PORT);
    });
})();