const express = require('express');
const router = express.Router();
const meetingControllers = require('../controllers/meetingControllers');

router.post('/createZoomMeeting',meetingControllers.createZoomMeeting);
module.exports = router;